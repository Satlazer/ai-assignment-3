﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum TileState
{
	Empty = 0,
	O,
	X
};

public enum Turn
{
	Player1 = 0,
	Player2 = 1
}

public class Board
{
	public TileState[,] tileState = new TileState[COLUMNS,ROWS];
	public Turn currentTurn = Turn.Player1;
	

	Vector2Int lastMove;
	List<Vector2Int> winningMove;
    public int numberOfMoves = 0;


	public int numOfNearComputerWins = 0;
	public int numOfNearHumanWins = 0;


	// --------------- //
	// CONST VARIABLES //
	// --------------- //
	public const int COLUMNS = 3;
	public const int ROWS = 3;
    const int MAX_SCORE = 100000;

	public bool IsFull()
	{
	    return (numberOfMoves == ROWS * COLUMNS);
	}

	public bool PlaceTile(Vector2Int loc, Turn whichPlayer)
	{
	    if (tileState[loc.x, loc.y] == TileState.Empty)
	    {
	        tileState[loc.x, loc.y] = (whichPlayer == Turn.Player1 ? TileState.X : TileState.O);
	        numberOfMoves += 1;
			lastMove = new Vector2Int(loc.x, loc.y);
	        return true;
	    }

	    // Couldn't drop a tile
	    return false;
	}

	public Board CreateCopy()
	{
	    Board newBoard = new Board();
	    newBoard.currentTurn = this.currentTurn;
	    newBoard.winningMove = this.winningMove;
	    newBoard.numberOfMoves = this.numberOfMoves;

	    for (int x = 0; x < COLUMNS; x++)
	    {
	        for (int y = 0; y < ROWS; y++)
	        {
	            newBoard.tileState[x,y] = this.tileState[x,y];
	        }
	    }

	    return newBoard;
	}

	
	public void ChangeTurns()
	{
		// Swap the turn
		if(currentTurn == Turn.Player1)
			currentTurn = Turn.Player2;
		else 
			currentTurn = Turn.Player1;
	}
	
	public int SimpleScoring()
	{
		numOfNearComputerWins = 0;
		numOfNearHumanWins = 0;

	    // Vertical points
	    for (int row = 0; row < ROWS - 2; row++) 
	    {
	        for (int column = 0; column < COLUMNS; column++) 
	        {
	            int score = ScorePosition(row, column, 1, 0);
	            if (score ==  MAX_SCORE) return score;
	            if (score == -MAX_SCORE) return score;
	        }
	    }

	    // Horizontal points
	    for (int row = 0; row < ROWS; row++) 
	    {
	        for (int column = 0; column < COLUMNS - 2; column++) 
	        {
				int score = ScorePosition(row, column, 0, 1);
				if (score ==  MAX_SCORE) return score;
				if (score == -MAX_SCORE) return score;
	        }
	    }

	    // Diagonal points 1 (left-bottom)

		for (int row = 0; row < ROWS - 2; row++)
		{
			for (int column = 0; column < COLUMNS - 2; column++)
			{
				int score = ScorePosition(row, column, 1, 1);
				if (score ==  MAX_SCORE) return score;
				if (score == -MAX_SCORE) return score;
			}
		}

	    // Diagonal points 2 (right-bottom)

		for (int row = 2; row < ROWS; row++)
		{
			for (int column = 0; column < COLUMNS - 2; column++)
			{
				int score = ScorePosition(row, column, -1, 1);
				if (score ==  MAX_SCORE) return score;
				if (score == -MAX_SCORE) return score;
			}
		}

	    return 0;
	}

	int ScorePosition(int row, int column, int delta_y, int delta_x)
	{
	    int human_points = 0;
	    int computer_points = 0;

	    // Determine score through amount of available chips
	    for (int i = 0; i < 3; i++)
	    {
			// This is so that it will still work with 2 AIs
			if(currentTurn == Turn.Player2)
			{
	        	if (tileState[column, row] == TileState.X)
	        	{
	        	    human_points++; // Add for each human chip
	        	}
	        	else if (tileState[column, row] == TileState.O)
	        	{
	        	    computer_points++; // Add for each computer chip
	        	}
			}
			else
			{
				if (tileState[column, row] == TileState.X)
	        	{
	        	    computer_points++; // Add for each human chip
	        	}
	        	else if (tileState[column, row] == TileState.O)
	        	{
	        	    human_points++; // Add for each computer chip
	        	}
			}

	        // Moving through our board
	        row += delta_y;
	        column += delta_x;
	    }

	    // If the human matched 3 in a row
	    if (human_points == 3)
	    {
	        return -MAX_SCORE;
	    }
	    else if (computer_points == 3)
	    {
	        return MAX_SCORE;
	    }
		else if (human_points == 2 && computer_points == 0)
		{
			++numOfNearHumanWins;
		}
		else if (computer_points == 2 && human_points == 0)
		{
			++numOfNearComputerWins;
		}
	    else
	    {
	    }
		// Return normal points
		return computer_points;
	}
}


public class TicTacToe : MonoBehaviour 
{
	public bool Player1AI = false;	
	public bool Player2AI = true;	
	[Range(1,10)]
	public int difficulty = 10;
	public GameObject PrefabX;
	public GameObject PrefabO;
	public GameObject TileParent;
	public Transform [] tilePosition;

	int numOfTurns = 0;
 
    
	const int NIL = -1; // Because NULL is 0 and we use 0
	Board board = new Board();

	bool win = false;
	public Material gridlineMaterial;

	List<AudioSource> audioSources = new List<AudioSource>();
	float timeToWait = 1.0f;
	float timeLeft = 1.0f;

	// Use this for initialization
	void Start () 
	{
		gridlineMaterial.SetColor("_Color", new Color(0.5f, 0.5f, 1.0f));
	}
	
	Vector3Int MaximizePlay(Board board, int depth)
	{
		// Call score of our board
    	int score = board.SimpleScoring();

    	// Break
    	if (board.IsFull() || depth == 0) return new Vector3Int(NIL, NIL, score);

    	// Column, Score
    	Vector3Int max = new Vector3Int(NIL, NIL, -99999);

    	// For all possible moves, create a new board and check where tiles can be placed
		for (int row = 0; row < Board.ROWS; row++)
    	{
    		for (int column = 0; column < Board.COLUMNS; column++)
    		{
			
    	    	Board newBoard = board.CreateCopy(); 

				// Check if a tile can be placed, and do it if so, then call the minimizePlay function
    	    	if (newBoard.PlaceTile(new Vector2Int(column, row), newBoard.currentTurn))
    	    	{
					newBoard.ChangeTurns();
    	    	    Vector3Int nextMove = MinimizePlay(newBoard, depth); // Recursive calling

    	    	    // Evaluate new move
    	    	    if ((max[0] == NIL && max[1] == NIL) || nextMove[2] >= max[2])
    	    	    {
    	    	        max[0] = column;
	    	            max[1] = row;
	    	            max[2] = nextMove[2];
    	    	    }
    	    	}
    		}
		}

    	return max;
	}

	Vector3Int MinimizePlay(Board board, int depth)
	{
		// Works the same as Maximize, just getting the min value instead
	    int score = board.SimpleScoring();

	    if (board.IsFull() || depth == 0) return new Vector3Int(NIL, NIL, score);

	    Vector3Int min = new Vector3Int(NIL, NIL, 99999);

	    for (int row = 0; row < Board.ROWS; row++)
    	{
    		for (int column = 0; column < Board.COLUMNS; column++)
    		{
	        	Board newBoard = board.CreateCopy();

	        	if (newBoard.PlaceTile(new Vector2Int(column, row), newBoard.currentTurn))
    	    	{
					newBoard.ChangeTurns();
	    	        Vector3Int nextMove = MaximizePlay(newBoard, depth);

	    	        if ((min[0] == NIL && min[1] == NIL) || nextMove[2] <= min[2])
	    	        {
	    	            min[0] = column;
	    	            min[1] = row;
	    	            min[2] = nextMove[2];
	    	        }
	    	    }
	    	}
		}
	    return min;
	}


	bool CheckWin(Vector2Int location, Turn playerTurn)
	{

		TileState tilePlayer;
		if(playerTurn == Turn.Player1)
		{
			tilePlayer = TileState.X;
		}
		else
		{
			tilePlayer = TileState.O;
		}

		// Check Horizontal
		{
			int score = 0;
			for(int i = 0; i < 3; ++i)
			{
				if(board.tileState[i,location.y] == tilePlayer)
					++score;
				if(score == 3)
					return true;

			}
		}
		
		// Check Vertical
		{
			int score = 0;
			for(int i = 0; i < 3; ++i)
			{
				if(board.tileState[location.x,i] == tilePlayer)
					++score;
				if(score == 3)
					return true;
			}
		}
		// Diagonals only need to be checked if it's even possible
		
		// Check Diagonals Up Right
		if(location.x == location.y)
		{
			int score = 0;
			for(int i = 0; i < 3; ++i)
			{
				if(board.tileState[i,i] == tilePlayer)
					++score;
				if(score == 3)
					return true;
			}
		}

		if(location.x + location.y == 2)
		{
			int score = 0;
			for(int i = 0; i < 3; ++i)
			{
				if(board.tileState[2-i,i] == tilePlayer)
					++score;
				if(score == 3)
					return true;
			}
		}		
		return false;
	}

	
	float turnStartTime = 0.0f;
	float turnTime = 0.0f;
	Vector2Int GenerateComputerDecision()
	{
		//if(numOfTurns < 2) // 
		//{
		//	if(board.tileState[1,1] == TileState.Empty)
		//	{
		//		return new Vector2Int(1, 1); 
		//	}
		//	else
		//	{
		//		//Debug.Log("RANDOM");
		//		return new Vector2Int(0, 0); 
		//	}
		//}
		//else
		//{
		//}

	    // Start the AI here
		turnStartTime = Time.realtimeSinceStartup;
	    Vector3Int aiMove = MaximizePlay(board, difficulty);
		turnTime = Time.realtimeSinceStartup;
		Debug.Log("Time: " + ((turnTime - turnStartTime) * 1000) + " Milliseconds");
	    return new Vector2Int(aiMove[0], aiMove[1]); 
	}

	void AddAudioSources(GameObject go)
	{
		AudioSource[] audioComponents = go.GetComponentsInChildren<AudioSource>(true);
		for(int i = 0; i < audioComponents.Length; ++i)
		{
			audioSources.Add(audioComponents[i]);
		}
	}

	void StopAudioSources()
	{
		for(int i = 0; i < audioSources.Count; ++i)
		{
			audioSources[i].Stop();
		}
	}

	void AddInstance(GameObject prefab, Vector2Int loc, TileState tileType)
	{
		//board.tileState[loc.x,loc.y] = tileType;
		StopAudioSources();
		board.PlaceTile(loc, board.currentTurn);
		
		
		//Debug.Log(board.numberOfMoves);
		Transform tileTransform = tilePosition[loc.x * 3 + loc.y];
		AddAudioSources(Instantiate(prefab, tileTransform.position, tileTransform.rotation));
		++numOfTurns;

		win = CheckWin(loc, board.currentTurn);
		if(win)
		{
			Debug.Log("WINNER");
			gridlineMaterial.SetColor("_Color", new Color(1.0f, 0.0f, 0.0f));
		}
	}

	void AddInstance(GameObject prefab, Transform trans, Vector2Int loc, TileState tileType)
	{
		//board.tileState[loc.y,loc.x] = tileType;
		StopAudioSources();
		board.PlaceTile(loc, board.currentTurn);
		//Debug.Log(board.numberOfMoves);
		Transform tileTransform = tilePosition[loc.x * 3 + loc.y];
		AddAudioSources(Instantiate(prefab, tileTransform.position, tileTransform.rotation));
		++numOfTurns;
	}

	void Place()
	{
		// Here we check where the player clicked to see if they hit a valid location, 
		// if so, check if that tile is empty, if it is, place a tile and check for victory
		RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	
        if (Physics.Raycast(ray, out hit)) 
		{
            if (hit.transform.tag == "Tile" )
			{
				Vector2Int loc = hit.transform.GetComponent<TicTacToeLocation>().location;
				loc = new Vector2Int(loc.y, loc.x);
				if(board.tileState[loc.x,loc.y] == TileState.Empty)
				{
					if(Player1AI == false && board.currentTurn == Turn.Player1)
					{
						AddInstance(PrefabX, hit.transform, loc, TileState.X);
					}
					else if(Player2AI == false && board.currentTurn == Turn.Player2)
					{
						AddInstance(PrefabO, hit.transform, loc, TileState.O);
					}

					win = CheckWin(loc, board.currentTurn);
					if(win)
					{
						Debug.Log("WINNER");
						gridlineMaterial.SetColor("_Color", new Color(0.0f, 1.0f, 0.0f));
						return;
					}
					if((board.currentTurn == Turn.Player1 || Player2AI == false) && (board.currentTurn == Turn.Player2 || Player1AI == false))
					{
						board.ChangeTurns();
						timeLeft = 0.0f;
						timeToWait = Random.Range(0.5f, 1.0f);
					}
				}
				else
				{
					Debug.Log("That tile is already owned!");
				}
			}
        }
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("Main", LoadSceneMode.Single);
		}

		if (Input.GetKeyDown(KeyCode.Q))
		{
			Application.Quit();
		}

		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			Player1AI = !Player1AI;
		}
		
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			Player2AI = !Player2AI;
		}

		if(Input.GetKeyDown(KeyCode.G))
		{
			board.SimpleScoring();
			Debug.Log("Number of near computer wins:\t" + board.numOfNearComputerWins);
			Debug.Log("Number of near human wins:\t" + board.numOfNearHumanWins);
		}

		if(!win && !board.IsFull())
		{
			timeLeft += Time.deltaTime;
			
			if((board.currentTurn == Turn.Player1 && Player1AI == true))
			{
				if(timeLeft >= timeToWait)
				{
					Vector2Int loc = GenerateComputerDecision();
					AddInstance(PrefabX, loc, TileState.X);
					board.ChangeTurns();
				}
			}
			else if(board.currentTurn == Turn.Player2 && Player2AI == true)
			{
				if(timeLeft >= timeToWait)
				{
					Vector2Int loc = GenerateComputerDecision();
					AddInstance(PrefabO, loc, TileState.O);
					board.ChangeTurns();
				}
			}
			else if (Input.GetMouseButtonDown(0)) 
			{
        		Place();
			}
		}
	}

	
}
